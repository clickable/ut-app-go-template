{% if cookiecutter.open_source_license == 'GNU General Public License v3' %}/*
 * Copyright (C) {% now 'local', '%Y'%}  {{cookiecutter.maintainer_name}}
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * ubuntu-calculator-app is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

{% endif %}import QtQuick 2.7
import Ubuntu.Components 1.3
//import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0

MainView {
    id: root
    objectName: 'mainView'
    applicationName: '{{cookiecutter.app_full_name|lower}}'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)

    Page {
        anchors.fill: parent

        header: PageHeader {
            id: header
            title: i18n.tr('{{cookiecutter.title}}')
        }

        ColumnLayout {
            spacing: units.gu(2)
            anchors {
                margins: units.gu(2)
                top: header.bottom
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            Item {
                id: spacer
                Layout.fillHeight: true
            }

            Label {
                text: testvar.message
                horizontalAlignment: Label.AlignHCenter
                Layout.fillWidth: true

                /*
                    To export go functions they have to be in uppercase but in qml
                    the first letter of the function has to be lower case.
                    The qml<->go bridge takes care of that.
                */
            }

            Label {
                text: testvar.output
                horizontalAlignment: Label.AlignHCenter
                Layout.fillWidth: true
            }

            Button {
                text: 'Do it!'
                color: 'green'
                onClicked: testvar.getMessage()
                Layout.alignment: Qt.AlignCenter
            }

            Item {
                id: spacer2
                Layout.fillHeight: true
            }
        }
    }
}
